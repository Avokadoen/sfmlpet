//
// Created by avokado on 23.12.17.
//

#include <SFML/System/Time.hpp>
#include <iostream>
#include "PhySprites.hpp"
#include "GLOBALS.hpp"


int PhySpriteCount = 0;
PhySprite::PhySprite() {
    std::cout << "\nPossible illegal PhySprite";
    psID = -9999;
}

PhySprite::PhySprite(int id) {
    psID = id;
}

PhySprite::PhySprite(sf::Texture& self, bool falling, sf::Vector2f pos) {
    std::cout << "\n\n NEW PHYSPRITE MADE " << 1 + PhySpriteCount << "\n\n";
    psID = ++PhySpriteCount;
    activePhysics = falling;
    force.x = force.y = 0;
    rotationVel = force;

    form.setTexture(self, true);
    form.scale(OBJECT_SCALE,OBJECT_SCALE);

    previousPos = pos;
    form.move(previousPos);
}

/* for applying force between frames */
void PhySprite::applyForce(sf::Vector2f extraForce){
    force += extraForce;
}

void PhySprite::update(sf::Time deltaTime){
    if(activePhysics) {
        sf::Vector2f velocity = force * deltaTime.asSeconds();
        force -= force * deltaTime.asSeconds();

        if (velocity.y <= 0) {
            velocity.y += GRAVITY * deltaTime.asSeconds();
        }
        previousPos = form.getPosition();
        form.move(velocity);
    }
}

void PhySprite::collided(PhySprite& p) {
    if (activePhysics && p.activePhysics) {
      sf::Vector2f f = p.force;
      p.force = force;
      force = f;
    }
    else{
        // TODO: possible dmg
        if(activePhysics){
          sf::Vector2f currentPos = form.getPosition();
          form.move(0, previousPos.y - currentPos.y);
          force.y = 0;
        }
        else if(p.activePhysics) {
          sf::Vector2f currentPos = p.form.getPosition();
          p.form.move(0, p.previousPos.y - currentPos.y);
          p.force.y = 0;
        }
    }
}

int PhySprite::valueOfID(){
    return psID;
}

bool PhySprite::operator == (const PhySprite& p ){return p.psID == psID;};

bool PhySprite::isActive(){ return activePhysics; }


sf::Vector2f PhySprite::getForce() { return force; }
