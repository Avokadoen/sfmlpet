//
// Created by avokado on 25.12.17.
//

#ifndef TESTSFML_ACTIVESPRITEOBJECTS_H
#define TESTSFML_ACTIVESPRITEOBJECTS_H

#endif //TESTSFML_ACTIVESPRITEOBJECTS_H

#include <vector>
#include <SFML/Graphics/RenderWindow.hpp>
#include "PhySprites.hpp"
#include "GLOBALS.hpp"


class SpriteController{
private:
    // NEW
    std::vector<int> updateIndex;
    std::vector<int> collisionActiveIndex;
    std::vector<int> drawIndex;
    PhySprite levelObjects[MAX_LEVEL_OBJECTS];
    unsigned int LOCount;
    sf::FloatRect activeRange; // TODO: Move to a view controller
    float collisionCheckRate;
public:
    SpriteController(sf::FloatRect activeRange);
    void add(PhySprite& newPS);
    void deleteObject(int checkID);
    void moveToInacive(PhySprite& newInactive);
    void moveToActive(int newIndex);
    void draw(sf::RenderWindow& w);
    void update(sf::Time deltaTime, sf::Vector2f viewPos);
    void globalCollision();
    void movePlayer(sf::Event& e);
};
