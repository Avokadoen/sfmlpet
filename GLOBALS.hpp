//
// Created by avokado on 23.12.17.
// Last update:

#ifndef TESTSFML_GLOBALS_H
#define TESTSFML_GLOBALS_H

#endif //TESTSFML_GLOBALS_H

#include <list>
#include <SFML/Graphics/Texture.hpp>

/*    type  name            value     */
const float GRAVITY         = 80;
const float OBJECT_SCALE    = 0.1;
const float COLLISION_RATE  = 0;
const int   MAX_LEVEL_OBJECTS  = 1000; // TODO: scale by loading from file
const int   WINDOW_X        = 800;
const int   WINDOW_Y        = 400;
const sf::Vector2f RENDER_RANGE(400, 800);
//const int   RENDER_RANGE_Y  = 1000;
