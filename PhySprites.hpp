//
// Created by avokado on 23.12.17.
//

#ifndef TESTSFML_PHYSPRITES_H
#define TESTSFML_PHYSPRITES_H

#endif //TESTSFML_PHYSPRITES_H

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Time.hpp>

class PhySprite{
private:
    int psID;
    bool activePhysics;
    sf::Vector2f previousPos;
    sf::Vector2f rotationVel;
    sf::Vector2f force;

public:
    sf::Sprite form; // TODO: interface
    PhySprite();
    PhySprite(int id);
    PhySprite(sf::Texture& self, bool falling, sf::Vector2f pos);
    void applyForce(sf::Vector2f extraForce);
    void update(sf::Time deltaTime);
    void collided(PhySprite& p);
    int valueOfID();
    bool operator==(const PhySprite& p);
    bool isActive();
    sf::Vector2f getForce();

};
