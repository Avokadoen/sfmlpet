#include <SFML/Graphics.hpp>
#include "SpriteController.hpp"
#include <iostream>
#include <string>


/* TODO:                                est */
// use segments of view to decide collision.
//  view controller
// Texture pool
// implement global queue for objects   70% *working
// use queue for draw and update        50% *working
// global queue for check of forces     20%
// input handling                       10%
// commenting                           0%
// character class
// file system for levels
// level building tools (lol)
/*                                          */

int main()
{
  sf::Font batmanForever;
  sf::Text fpsCounter;
  sf::Clock deltaClock;
  sf::Time deltaTime;
  sf::View view(sf::Vector2f(WINDOW_X/2, WINDOW_Y/2),  sf::Vector2f(WINDOW_X, WINDOW_Y));
  sf::RenderWindow window(sf::VideoMode(WINDOW_X, WINDOW_Y), "something!");

  /* TODO: active range should not change based on resolution
   * - does it with this code though?
   * */
  view.setViewport(sf::FloatRect(0.f, 0.f, 1.f, 1.f));
  sf::FloatRect activeRange = sf::View(window.getDefaultView()).getViewport();

  // TODO: activeRange need to bound to view


  // TODO: remove
  SpriteController testController(activeRange);

  sf::Texture removeMeTestPlayer;
  sf::Texture removeMeTestFloor;
  removeMeTestPlayer.loadFromFile("nicubunu-Stickman-13.png");
  removeMeTestFloor.loadFromFile("s2.png");
  batmanForever.loadFromFile("batmfa__.ttf");

  fpsCounter.setFont(batmanForever);
  fpsCounter.setPosition(WINDOW_X - 100, 20);
  fpsCounter.setScale(0.5, 0.5);

  PhySprite player(removeMeTestPlayer, true, sf::Vector2f(0, -50));
  testController.add(player);

  int distance = 0;
  PhySprite* platform;
  for(int i = 0; i <= 32; i++){  // STRESSTEST
    platform = new PhySprite(removeMeTestFloor, false, sf::Vector2f(distance+=20, 300));
    testController.add(*platform);
  }

  int fps = 0;
  int fc = 0;
  float second = 0;
  while (window.isOpen())
  {
      sf::Event event;

      while (window.pollEvent(event))
      {
          if (event.type == sf::Event::Closed)
              window.close();
          else if (event.type == sf::Event::Resized)
          {
             // TODO: Scale world after window

              std::cout << "new width: " << event.size.width << std::endl;
              std::cout << "new height: " << event.size.height << std::endl;

          }
          else if (event.type == sf::Event::LostFocus)
              deltaTime = deltaTime.Zero;

          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
          {
              // left key is pressed: move our character
              //levelObjects[0].applyForce(sf::Vector2f(0,-400));
          }
          else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
          {
              // left key is pressed: move our character
              //levelObjects[0].applyForce(sf::Vector2f(100,0));
          }
          else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
          {
              // left key is pressed: move our character
              //levelObjects[0].applyForce(sf::Vector2f(-100,0));
          }
          testController.movePlayer(event);
      }
      fc += 1;
      second += deltaTime.asSeconds();
      window.setView(view);
      window.clear();

      testController.update(deltaTime, view.getCenter());
      testController.draw(window);
      if(second >= 1){
        fps = fc;
        fc = 0;
        second -= 1;
      }
      fpsCounter.setString(std::to_string(fps));
      window.draw(fpsCounter);
      window.display();

      deltaTime = deltaClock.restart();

  }

  return 0;
}
