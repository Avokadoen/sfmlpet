//
// Created by avokado on 25.12.17.
//

#include "SpriteController.hpp"
#include <iostream>
#include <set>

SpriteController::SpriteController(sf::FloatRect aRange) {
    LOCount = collisionCheckRate = 0;
    activeRange = aRange;
}

void SpriteController::add(PhySprite& newPS) {
    std::cout << newPS.valueOfID() << std::endl;
    levelObjects[LOCount++] = newPS;
    std::cout << newPS.valueOfID() << std::endl;
    std::cout << levelObjects[LOCount+1].valueOfID() << std::endl;
}

  //TODO:
void SpriteController::deleteObject(int checkId){
}

void SpriteController::moveToInacive(PhySprite& newInactive){
}

void SpriteController::moveToActive(int newIndex){
    std::cout << "\nmoving to active";

    // if(!levelObjects[newIndex].isActive()){
    //   collisionInactiveIndex.push_back(newIndex);
    // }else{
    //   collisionActiveIndex.push_back(newIndex);
    // }
    updateIndex.push_back(newIndex);
    drawIndex.push_back(newIndex);
}

void SpriteController::draw(sf::RenderWindow& w) {
    int cIndex = -1;
    while (!drawIndex.empty()){
      cIndex = drawIndex.back();
      drawIndex.pop_back();
      std::cout << "\nDrawing id: " << levelObjects[cIndex].valueOfID();
      w.draw(levelObjects[cIndex].form);

    }
}

void SpriteController::update(sf::Time deltaTime, sf::Vector2f viewPos) {

    // TODO: Active should be based on position, not intersects.
     std::cout << "\nUpdating vectors";
     sf::Vector2f lessThan = viewPos + RENDER_RANGE;
     sf::Vector2f greaterThan = viewPos - RENDER_RANGE;

     for (unsigned int i = 0; i < LOCount; i++){
       sf::Vector2f viewOrigin = levelObjects[i].form.getOrigin();

       if(viewOrigin.x >= greaterThan.x && viewOrigin.y >= greaterThan.y
          && viewOrigin.x <= lessThan.x && viewOrigin.x <= lessThan.x){

         moveToActive(i);
       }
     }

    std::cout << "\ncollision timer";
    collisionCheckRate += deltaTime.asSeconds();
    if(collisionCheckRate >= COLLISION_RATE){
        std::cout << "\nChecking global collision";
        collisionCheckRate -= COLLISION_RATE;
        collisionActiveIndex = updateIndex;
        globalCollision();
    }
    int cIndex = -1;
    while (!updateIndex.empty()){
      cIndex = updateIndex.back();
      updateIndex.pop_back();
      std::cout << "\nupdate active with id " << levelObjects[cIndex].valueOfID();
      levelObjects[cIndex].update(deltaTime);
    }

}

void SpriteController::globalCollision() {
  // TODO:
  std::cout << "\ncollisionActiveIndex:";
  for(auto&& it : collisionActiveIndex){
  std::cout << " " << it << " ";
  }
  int count1 = 0;
  // int count2 = 1;
  for (auto active=collisionActiveIndex.begin(); active != collisionActiveIndex.end(); ++active){
    std::cout << "\ncheck one collision: " << count1++;
    if(levelObjects[*active].isActive()){
      for (auto check=collisionActiveIndex.begin(); check != collisionActiveIndex.end(); ++check){
        if((std::distance(active, check) > 0 || !levelObjects[*check].isActive()) &&
          levelObjects[*active].form.getGlobalBounds().intersects(levelObjects[*check].form.getGlobalBounds())){
          levelObjects[*active].collided(levelObjects[*check]);
        }
      }
    }
//     auto checkActive = active;
//     count2 = count1;
//     do{
//       std::cout << "\nat: " << count2++;
//       if(levelObjects[*main].form.getGlobalBounds().intersects(levelObjects[*checkActive].form.getGlobalBounds())
//         && main != checkActive){
//           levelObjects[*main].collided(levelObjects[*checkActive]);
//       }
//       checkActive++;
//     }while(checkActive != collisionActiveIndex.end());
  }
}

void SpriteController::movePlayer(sf::Event& e){

  // if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
  // {
  //     // left key is pressed: move our character
  //     levelObjects[0].applyForce(sf::Vector2f(0,-400));
  // }
  // else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
  // {
  //     // left key is pressed: move our character
  //     levelObjects[0].applyForce(sf::Vector2f(100,0));
  // }
  // else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
  // {
  //     // left key is pressed: move our character
  //     levelObjects[0].applyForce(sf::Vector2f(-100,0));
  // }
}
